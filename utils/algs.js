export default class Graph {
  constructor () {
    this.edges = {}
    this.nodes = []
  }

  addNode (node) {
    this.nodes.push(node)
    this.edges[node] = {}
  }

  addEdge (node1, node2, weight = 1) {
    this.edges[node1][node2] = weight
    this.edges[node2][node1] = weight
  }

  addDirectedEdge (node1, node2, weight = 1) {
    this.edges[node1][node2] = weight
  }

  display () {
    let graph = ''
    this.nodes.forEach((node) => {
      graph += node + '->' + Object.keys(this.edges[node]).join(', ') + '\n'
    })
    console.log(graph)
  }

  floydWarshall () {
    const dist = []
    const next = []
    const N = this.nodes.length
    for (let i = 0; i < N; i++) {
      dist[i] = []
      next[i] = []
      for (let j = 0; j < N; j++) {
        dist[i][j] = this.edges[this.nodes[i]][this.nodes[j]]
        next[i][j] = j
        if (dist[i][j] === undefined) {
          dist[i][j] = Infinity
          next[i][j] = -1
        }
        if (i === j) {
          dist[i][j] = 0
        }
      }
    }
    for (let k = 0; k < N; k++) {
      for (let i = 0; i < N; i++) {
        for (let j = 0; j < N; j++) {
          if (dist[i][j] > dist[i][k] + dist[k][j]) {
            dist[i][j] = dist[i][k] + dist[k][j]
            next[i][j] = next[i][k]
          }
        }
      }
    }
    return {
      next,
      dist
    }
  }
}
