import SessionWorker from '~/assets/js/algoritms.worker'

function Worker () {
  const worker = new SessionWorker()
  return Object.seal({
    message (data, transfer) {
      worker.postMessage(data, transfer)
    },
    listen (f) {
      worker.addEventListener('message', f)
    },
    unlisten (f) {
      worker.removeEventListener('message', f)
    },
    destroy () {
      worker.terminate()
    }
  })
}

export default (context, inject) => {
  inject('worker', {
    createWorker () {
      return new Worker()
    }
  })
}
