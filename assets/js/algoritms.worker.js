import Graph from '@/utils/algs'

function transform (nodes, edges, directed = true) {
  const g = new Graph()
  for (let i = 0; i < nodes.length; i++) {
    g.addNode(nodes[i].data.placeId)
  }
  for (let i = 0; i < edges.length; i++) {
    const e = edges[i]
    if (!directed) {
      g.addEdge(e.data.source, e.data.target, e.data.weight)
    } else {
      g.addDirectedEdge(e.data.source, e.data.target, e.data.weight)
    }
  }
  return g
}

function getPath (nodes, next) {
  const indexOf = (id) => {
    return nodes.indexOf(id)
  }
  let u = indexOf('I')
  const v = indexOf('F')
  const path = []
  if (u === -1) {
    return []
  }
  if (next[u][v] === -1) { return path }
  path.push(u)
  while (u !== v) {
    u = next[u][v]
    path.push(u)
  }
  return path
}

function init ({ nodes, edges, directed }) {
  const g = transform(nodes, edges, directed)
  const { dist, next } = g.floydWarshall()
  const path = getPath(g.nodes, next)
  return { dist, next, nodes: g.nodes, path }
}

// Respond to message from parent thread
self.addEventListener('message', (event) => {
  self.postMessage(JSON.stringify(init(event.data)))
})
